import { boot } from 'quasar/wrappers'
import productModule from "src/modules/product";
import cartModule from "src/modules/cart";
import {registerModules} from "src/register-modules";

registerModules({
  product: productModule,
  cart: cartModule
})
// let routerInstance = void 0
// export default async ({ router }) => {
//   // something to do
//   routerInstance = router
// }
// export { routerInstance }

// var routerInstance;
// "async" is optional;
// // more info on params: https://v2.quasar.dev/quasar-cli/boot-files
// export default boot(async ({ app, router} ) => {
//   return router;
// // console.log('Modules are registered!');
//   // something to do
// })
// console.log(routerInstance)
//
// export {routerInstance}
