import router from "./router";
import { route } from 'quasar/wrappers'
// import {useRouter} from 'vue-router'
// import store from "./store";
// const router = useRouter();
// console.log(router)
const registerModule = (name, module) => {
  // if (module.store) {
  //   store.registerModule(name, module.store);
  // }
  // if (module.router) {
  //   console.log(module);
  //   // module.router(router);
  // }
};

export const registerModules = (modules) => {
  // console.log(modules)
  Object.keys(modules).forEach((moduleKey) => {
    const module = modules[moduleKey];
    registerModule(moduleKey, module);
  });
};
