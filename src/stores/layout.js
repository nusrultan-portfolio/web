import {defineStore} from 'pinia'

export const useLayoutStore = defineStore('layout', {
  state: () => ({
    _leftDrawerOpen: false
  }),

  getters: {
    leftDrawerOpen: (state) => {
      return state._leftDrawerOpen
    }
  },

  actions: {
    toggleLeftDrawer() {
      this._leftDrawerOpen = !this._leftDrawerOpen
    }
  }
})
