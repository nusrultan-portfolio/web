import {defineStore} from 'pinia'

export const useLayoutPersistStore = defineStore('layout-persist', {
  state: () => ({
    _leftDrawerOpen: false
  }),

  getters: {
    leftDrawerOpen: (state) => {
      return state._leftDrawerOpen
    }
  },

  actions: {
    toggleLeftDrawer() {
      this._leftDrawerOpen = !this._leftDrawerOpen
    }
  }
})
