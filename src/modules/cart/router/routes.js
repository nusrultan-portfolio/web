const routes = [
  {
    path: '/my-cart',
    component: () => import('src/modules/cart/layouts/MyCartMaster.vue'),
    name: 'MyCartMaster',
    children: [
      {
        path: '',
        component: () => import('src/modules/cart/pages/MyCart.vue'),
        name: 'MyCart'
      }
    ]
  }
]

export default routes
