import routes from './routes'
import Router from 'src/router'

routes.forEach(route => {
  Router.addRoute(route)
})
export default Router;
