import routes from './routes'
import router from 'src/router'

routes.forEach(route => {
  router.addRoute(route)
})
export default router
