
const routes = [
  {
    path: '/products',
    component: () => import('src/modules/product/layouts/ProductMaster.vue'),
    name: 'ProductMaster',
    children: [
      {
        path: '',
        component: () => import('src/modules/product/pages/ProductList.vue'),
        name: 'ProductList'
      }
    ]
  }
]

export default routes
