import { defineStore } from 'pinia'

export const useProductStore = defineStore('product', {
  persist: true,
  state: () => ({
    _products: ['Product 1', 'Product 2']
  }),

  getters: {
    products: (state) => {
      return state._products;
    }
  },

  actions: {
  }
})
